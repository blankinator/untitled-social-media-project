from unittest import TestCase

from untitled_social_network_project.utils.name_generator import get_name_set


class NameGeneratorTester(TestCase):

    def test_name_generation(self):
        length = 200

        names = get_name_set(length)

        self.assertIsNotNone(names)
        self.assertTrue(len(names) == 200)
