import math

import names as nms

male_to_female_ratio = 101 / 100


def get_name_set(length: int = 200) -> set:
    """
    Returns a set of names that tries to match both name and gender distribution from the real world
    :param length: length of the set to return
    :return: a set of names
    """

    # TODO: beter implementation of real world statistical distribution

    sex_cutoff = math.floor(length * male_to_female_ratio)

    def get_set_by_gender(length: int, gender: str, name_set: set = None) -> set:
        """ returns a set with n unique names of a certain gender """

        # initialize empty set, if none is given
        name_set = name_set if name_set is not None else set()

        if len(name_set) == length:
            return name_set

        new_name_set = name_set.copy()
        new_name_set.add(nms.get_first_name(gender))

        return get_set_by_gender(length, gender, new_name_set)

    male_names = get_set_by_gender(sex_cutoff, "male")
    female_names = get_set_by_gender(length - sex_cutoff, "female")

    names = male_names + female_names
    return names
